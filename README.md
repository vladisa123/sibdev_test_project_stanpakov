# SIBDEV TEST API VLADISLAV STANPAKOV


## How to set up the project:
* Copy the project using:
```
  https://gitlab.com/vladisa123/sibdev_test_project_stanpakov.git
```

* Go to folder:
```bash
  cd sibdev_test_project
```

* Run the command (to start the project in the detach mode):
```bash
  docker-compose up -d 
```

* The admin dashboard will be available at
```bash
  http://localhost/admin
```

## API SWAGGER DOCS
```bash
  http://localhost
```

## API REDOC DOCS
```bash
  http://localhost/redoc
```