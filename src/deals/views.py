from django.core.cache import cache
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import generics, status
from rest_framework.response import Response

from .models import Deal
from .serializers import TopClientsSerializer, FileUploadSerializer, DealSerializer
from .services.deals_processing import clear_last_deals_list
from .services.file_processing import csv_file_parser
from .services.top_clients import top_clients_queryset


class TopClientListView(generics.ListAPIView):
    """ Endpoint: returns top clients list  """

    queryset = top_clients_queryset()
    serializer_class = TopClientsSerializer

    @method_decorator(cache_page(60))
    def get(self, request, *args, **kwargs):
        data = {
            "response": self.serializer_class(self.queryset.all(), many=True).data
        }
        return Response(data, status=200)


class UploadFileView(generics.CreateAPIView):
    """ Endpoint: check and parse deals from file; return: "OK" on success """

    queryset = Deal.objects.all()
    serializer_class = FileUploadSerializer

    def post(self, request, *args, **kwargs):
        serializer_file = FileUploadSerializer(data=request.data)
        serializer_file.is_valid(raise_exception=True)

        data = csv_file_parser(serializer_file.validated_data['file'])
        serializer_deal = DealSerializer(data=data, many=True)

        if serializer_deal.is_valid():
            clear_last_deals_list()
            serializer_deal.save()
            cache.clear()
            return Response(
                data={"status": "OK"},
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                data={
                    "status": "Error",
                    "Desc.": f"{serializer_deal.errors}"
                             f" - в процессе обработки файла произошла ошибка "
                },
                status=status.HTTP_400_BAD_REQUEST
            )
