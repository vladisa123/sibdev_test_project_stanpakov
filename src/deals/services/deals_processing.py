from ..models import Deal


def clear_last_deals_list() -> None:
    Deal.objects.all().delete()
