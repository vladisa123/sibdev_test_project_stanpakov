import codecs
import csv


def csv_file_parser(file) -> list:
    return list(csv.DictReader(codecs.iterdecode(file, "utf-8"), delimiter=","))