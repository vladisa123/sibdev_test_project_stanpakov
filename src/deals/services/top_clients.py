import os

from django.db.models import QuerySet, Sum

from ..models import Client


def top_clients_queryset() -> QuerySet:
    """ Makes top clients queryset """

    count = int(os.getenv("TOP_CLIENTS_DISPLAY_COUNT"))
    return Client.objects.annotate(spent_money=Sum('deal__total')).order_by('-spent_money')[:count]


def top_clients_equal_gems_list() -> set:
    """ Get equal gems list of top clients """

    users_gems = []
    for client in top_clients_queryset():
        users_gems += client.gems.values_list('title').distinct('title')
    return set([gem[0] for gem in users_gems if users_gems.count(gem) > 1])
