from django.urls import path

from .views import *

urlpatterns = [
    path('top_clients_list/', TopClientListView.as_view()),
    path('upload_deals_file/', UploadFileView.as_view())
]