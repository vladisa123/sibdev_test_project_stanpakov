from django.db import models


class Gem(models.Model):
    title = models.CharField(max_length=250)

    def __str__(self):
        return self.title


class Client(models.Model):
    username = models.CharField(max_length=250)
    gems = models.ManyToManyField(Gem, through="Deal")

    def __str__(self):
        return self.username


class Deal(models.Model):
    customer = models.ForeignKey(Client, on_delete=models.DO_NOTHING)
    item = models.ForeignKey(Gem, on_delete=models.DO_NOTHING)
    total = models.IntegerField()
    quantity = models.IntegerField()
    date = models.DateTimeField()
