from django.contrib import admin

from .models import Deal, Client


@admin.register(Deal)
class DealAdmin(admin.ModelAdmin):
    list_display = ("customer", "item", "total", "quantity", "date",)
    list_display_links = ("customer",)


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ("username",)
    list_display_links = ("username",)
