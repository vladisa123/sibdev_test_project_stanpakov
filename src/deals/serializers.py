from django.core.validators import FileExtensionValidator
from rest_framework import serializers

from .models import Client, Deal, Gem
from .services.top_clients import top_clients_equal_gems_list


class TopClientsSerializer(serializers.ModelSerializer):
    """ Top clients list serializer """

    gems = serializers.StringRelatedField(many=True)
    spent_money = serializers.IntegerField()

    def to_representation(self, instance):
        """ Return only equal gems list """

        ret = super().to_representation(instance)
        ret['gems'] = set(ret['gems'])
        ret['gems'] = [gem for gem in top_clients_equal_gems_list() if gem in ret['gems']]
        return ret

    class Meta:
        model = Client
        fields = ('username', 'spent_money', 'gems')


class DealSerializer(serializers.ModelSerializer):
    """ Deal model serializer """

    customer = serializers.CharField()
    item = serializers.CharField()

    def create(self, validated_data):
        validated_data['customer'] = (Client.objects.get_or_create(username=validated_data['customer']))[0]
        validated_data['item'] = (Gem.objects.get_or_create(title=validated_data['item']))[0]
        return Deal.objects.create(**validated_data)

    class Meta:
        model = Deal
        fields = "__all__"


class FileUploadSerializer(serializers.Serializer):
    """ Uploaded file serializer"""

    file = serializers.FileField(validators=[FileExtensionValidator(['csv'])])
